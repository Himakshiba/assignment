if False > True:
    print 'False is less than True'
else:
    print 'False is greater than True'
if False > -1:
    print 'False is greater than -1'
if True < 2:
    print 'True is less than 2'
if True == 1:
    print 'True is 1'
else:
    print 'True is something else'
if False <> 0:
    print 'False is something else'
else:
    print 'False is 0'
raw_input()